#!/usr/bin/perl
use strict;
use warnings;
use Mail::Address;

my (@lines, $dest, $fh, $hadsub, $hadfrom, $dir, $mboxsep);
$mboxsep = "From foo\@bar.tld Mon Sep 17 00:00:00 2001\n";

if ($#ARGV == 0) {
    $dir = $ARGV[0];
    if (!-e $dir) {
        mkdir $dir || warn "Could not create directory: \"$dir\".\n";
    } else {
        if (!-d $dir) {
            warn "\"$dir\" exists, but is not a directory. Ignoring.\n";
            $dir = '';
        }
    }
} else {
    $dir = '';
}

push @lines, $mboxsep;
$hadsub = $hadfrom = 0;
while (my $line = <STDIN>) {
    if ($line =~ m/^--\@\@\+\+PATCH-PL-SEPERATOR\+\+\@\@--/) {
        push @lines, $mboxsep;
    } else {
        push @lines, $line;
    }
    if (!$hadsub && $line =~ s/^Subject:\s+//) {
        $hadsub = 1;
        chomp $line;
        chomp $line;
        $line =~ s/^\[PATCH[^\]]*\]\s+//;
        $line =~ s/[^[:ascii:]]+/_/g;
        $line =~ s/\s+/_/g;
        $line =~ s/[.:\/]+/_/g;
        $line =~ s/\.+$//;
        if (defined $dest) {
            $dest = "$dest$line.patch";
        } else {
            $dest = "$line.patch";
        }
    } elsif (!$hadfrom && $line =~ s/^From:\s+//) {
        $hadfrom = 1;
        my (@a, $addr);
        @a = Mail::Address->parse($line);
        $addr = $a[0]->phrase;
        $addr =~ s/\s+/_/g;;
        if (defined $dest) {
            $dest = "$addr-$dest";
        } else {
            $dest = "$addr-";
        }
    }
}
if (!defined $dest || $dest eq '') {
    my $num = 0;
    my $fb = 'fallback-';
    while (-e "$fb$num.patch") {
        $num++;
    }
    $dest = "$fb$num.patch";
}
if ($lines[$#lines] =~ m/^From\s+/) {
    $lines[$#lines] = '';;
}

$dest = "$dir/$dest";
print "Writing patch: \"$dest\"\n";
open $fh, ">", "$dest" or die "Could not open \"$dest\": $!\n";
foreach my $line (@lines) {
    print {$fh} "$line";
}
close $fh;

# To make sure mutt doesn't just go on:
exit 1
