use strict;

sub escape_regex_specials($) {
  my $string = shift;
  $string =~ s/([\.\$])/\\$1/g;
  return $string;
}

sub check_address($$) {
  my $address = shift;
  my $killfile=shift;
  my $line;
  open(THREAD_IGN_FILE, "<$killfile") or die("check_address(): could not open '$killfile': $!\n");
  $address = &escape_regex_specials($address);
  while ($line = <THREAD_IGN_FILE>) {
    chomp($line);
    if ($line eq $address) {
      close(THREAD_IGN_FILE);
      return 1;
    }
  }
  close(THREAD_IGN_FILE);
  return 0;
}

sub ui($$$) {
  my $question=shift;
  my $already=shift;
  my $killfile=shift;
  my $msgid;
  my $in;

  $msgid=<STDIN>;

  if (!$msgid) {
    print("No input! -Press _ENTER_ to exit-\n");
    close(STDIN);
    open(STDIN, "</dev/tty");
    <STDIN>;
    exit 0;
  }

  chomp($msgid);
  if (!&check_address($msgid, $killfile)) {
    print("$question '$msgid'?\n");
    $msgid = &escape_regex_specials($msgid);
    close(STDIN);
    open(STDIN, "</dev/tty");
    chomp($in=<STDIN>);
    if ($in eq 'y' || $in eq 'yes') {
      #print "$msgid\n";
      open(THREAD_IGN_FILE, ">>$killfile") or die("could not open '$killfile': $!\n");
      print(THREAD_IGN_FILE "$msgid\n");
      close(THREAD_IGN_FILE);
    }
  }
  else {
    print("'$msgid' $already! -press _ENTER_ to continue-");
    close(STDIN);
    open(STDIN, "</dev/tty");
    <STDIN>;
  }
}
1;
